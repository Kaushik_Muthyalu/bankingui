import { useContext, useEffect } from "react";
import { authenticate, loginDetails, refreshToken } from "../api/authenticate";
import { registerUser, registrationdetails } from "../api/register";
import { AuthContext } from "../context/AuthContext";
import { Action } from "../reducers/authReducer";

const useAuth = () => {
  const authContext = useContext(AuthContext);

  useEffect(() => {
    if (authContext.state.refresh_token) {
      storeToken();
    }
  }, [authContext.state.refresh_token]);

  const storeToken = () => {
    localStorage.setItem("refresh_token", authContext.state.refresh_token!);
  };

  const init = async (dispatch: React.Dispatch<Action>) => {
    const token = localStorage.getItem("refresh_token");
    if (token != null) {
      const result = await refreshToken(token ?? authContext.state.refresh_token!);
      dispatch({ type: "login", payload: result });
    }
  };

  const register = async (registration: registrationdetails) => {
    const result = await registerUser(registration);
    authContext.dispatch({ type: "login", payload: result });
  };

  const login = async (login: loginDetails) => {
    const result = await authenticate(login);
    authContext.dispatch({ type: "login", payload: result });
  };

  const refresh = async (token?: string) => {
    const result = await refreshToken(token ?? authContext.state.refresh_token!);
    authContext.dispatch({ type: "login", payload: result });
  };

  const logout = async () => {
    authContext.dispatch({ type: "logout" });
    localStorage.clear();
  };

  return {
    init: init,
    register: register,
    login: login,
    refresh: refresh,
    logout: logout,
  };
};

export default useAuth;
