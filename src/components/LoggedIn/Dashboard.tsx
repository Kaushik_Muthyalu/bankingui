import React from "react";
import Container from "../common/Layout/Container";
import TransactionList from "./TransactionList";
import UserInfo from "./UserInfo";

const Dashboard = () => {
  return (
    <Container>
      <UserInfo />
      <TransactionList />
    </Container>
  );
};

export default Dashboard;
