import { useContext, useEffect, useState } from "react";
import { getAllTransactions, deposit, withdraw } from "../../api/transaction";
import { AuthContext } from "../../context/AuthContext";
import Button from "../common/Button";
import Card, { CardContent, CardTitle } from "../common/Card";
import Col from "../common/Layout/Col";
import Row from "../common/Layout/Row";
import Section from "../common/Layout/Section";
import Report from "../common/Report";
import TextInput from "../common/TextInput";

interface Transaction {
  Id: number;
  Amount: number;
  TransactionType: string;
  TransactionTime: string;
}

const TransactionList = () => {
  const [transactions, setTransactions] = useState<Transaction[]>([]);
  const [amount, setAmount] = useState(0);

  const authContext = useContext(AuthContext);

  useEffect(() => {
    getAllTransactions(authContext.state.access_token ?? "").then((data) => {
      setTransactions(data);
    });
  }, []);

  const onDeposit = async () => {
    const transaction = await deposit(authContext.state.access_token!, { Amount: amount });
    if (transaction) {
      setTransactions([...transactions, transaction]);
      setAmount(0);
    }
  };

  const onWithdraw = async () => {
    const transaction = await withdraw(authContext.state.access_token!, { Amount: amount });
    if (transaction) {
      setTransactions([...transactions, transaction]);
      setAmount(0);
    }
  };
  return (
    <Section>
      <Card>
        <CardContent>
          <CardTitle className="center">Transactions</CardTitle>
          <Section>
            <Report>
              <table>
                <thead>
                  <tr>
                    <th>Amount</th>
                    <th>Transaction Type</th>
                    <th>Time</th>
                  </tr>
                </thead>

                <tbody>
                  {transactions.map((e) => (
                    <tr key={e.Id}>
                      <td>{e.Amount}</td>
                      <td>{e.TransactionType}</td>
                      <td>{e.TransactionTime}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </Report>
          </Section>
          <Section>
            <Row>
              <TextInput
                col={{ s: 4 }}
                value={amount}
                onChange={(e) => setAmount(Number.parseFloat(e.target.value))}
                type="number">
                Amount
              </TextInput>
              <Button type="button" onClick={() => onDeposit()} col={{ s: 3 }} offset={{ s: 1 }}>
                Deposit
              </Button>
              <Button
                type="button"
                onClick={() => onWithdraw()}
                background={{ name: "red" }}
                col={{ s: 3 }}
                offset={{ s: 1 }}>
                Withdraw
              </Button>
            </Row>
          </Section>
        </CardContent>
      </Card>
    </Section>
  );
};

export default TransactionList;
