import { Fragment } from "react";
import { NavLink } from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import Container from "../common/Layout/Container";
import Navbar from "../common/Navbar";
import BrandLogo from "../common/Navbar/BrandLogo";

type LayoutProps = { children?: any };

const Layout = (props: LayoutProps) => {
  const auth = useAuth();
  return (
    <Fragment>
      <div style={{ height: "100vw", width: "100hw", position: "fixed" }}></div>
      <Navbar id="loggedInNavbar">
        <Container>
          <BrandLogo>BankingApp</BrandLogo>
          <ul id="nav-mobile" className="right hide-on-med-and-down black-text">
            <li>
              <a
                href="/#"
                onClick={() => {
                  auth.logout();
                }}>
                Logout
              </a>
            </li>
          </ul>
        </Container>
      </Navbar>
      {props.children}
    </Fragment>
  );
};
export default Layout;
