import { Redirect, Route, Switch } from "react-router";
import Dashboard from "./Dashboard";
import Layout from "./Layout";

interface LoggedInProps {
  children?: any;
}

const LoggedIn = (props: LoggedInProps) => {
  return (
    <Layout>
      <Dashboard />
    </Layout>
  );
};
export default LoggedIn;
