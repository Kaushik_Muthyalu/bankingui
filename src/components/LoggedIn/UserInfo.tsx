import { useContext, useEffect, useState } from "react";
import { getUserInfo } from "../../api/user";
import { AuthContext } from "../../context/AuthContext";
import Card, { CardContent, CardTitle } from "../common/Card";
import Col from "../common/Layout/Col";
import Row from "../common/Layout/Row";
import Section from "../common/Layout/Section";
import Report from "../common/Report";

interface User {
  Name: string;
  Email: string;
  DOB: string;
  Balance: number;
}

const UserInfo = () => {
  const [user, setUser] = useState<User>({ Name: "", Email: "", DOB: "", Balance: 0 });

  const authContext = useContext(AuthContext);

  useEffect(() => {
    getUserInfo(authContext.state.access_token ?? "").then((data) => {
      setUser(data);
    });
  }, []);

  return (
    <Section>
      <Card>
        <CardContent>
          <CardTitle className="center">User Info</CardTitle>
          <Report>
            <table>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>DOB</th>
                  <th>Balance</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{user.Name}</td>
                  <td>{user.Email}</td>
                  <td>{user.DOB}</td>
                  <td>{user.Balance}</td>
                </tr>
              </tbody>
            </table>
          </Report>
        </CardContent>
      </Card>
    </Section>
  );
};

export default UserInfo;
