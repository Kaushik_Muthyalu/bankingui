import { Fragment, useReducer } from "react";
import useAuth from "../../hooks/useAuth";
import { formReducer } from "../../reducers/formReducer";
import Button from "../common/Button";
import Card from "../common/Card";
import { Form } from "../common/Form";
import Col from "../common/Layout/Col";
import Container from "../common/Layout/Container";
import Row from "../common/Layout/Row";
import TextInput from "../common/TextInput";

const Login = () => {
  const [state, dispatch] = useReducer(formReducer, { Email: "", Password: "" });

  const auth = useAuth();

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    auth.login(state);
  };

  return (
    <Fragment>
      <div className="center-align">
        <Container style={{ marginTop: "8em" }}>
          <Card
            className=" z-depth-1 logout-secondary-bg"
            style={{ display: "inline-block", padding: "32px 48px 0px 48px" }}>
            <Form col={{ s: 12 }} onSubmit={(e) => onSubmit(e)}>
              <Row>
                <Col col={{ s: 12 }}>Enter your credentials</Col>
              </Row>

              <Row>
                <TextInput
                  col={{ s: 12 }}
                  value={state.Email}
                  type="email"
                  name="email"
                  id="email"
                  onChange={(e) => {
                    dispatch({ key: "Email", value: e.target.value });
                  }}
                  required
                  validate>
                  Email
                </TextInput>
              </Row>

              <Row>
                <TextInput
                  col={{ s: 12 }}
                  value={state.Password}
                  type="password"
                  name="password"
                  id="password"
                  onChange={(e) => {
                    dispatch({ key: "Password", value: e.target.value });
                  }}
                  required
                  validate>
                  Password
                </TextInput>
              </Row>

              <br />
              <div className="center-align">
                <Row>
                  <Button name="btn_login" col={{ s: 12 }} large waves wavesLight formNoValidate>
                    Login
                  </Button>
                </Row>
              </div>
            </Form>
          </Card>
        </Container>
        <a href="#!">Create account</a>
      </div>
    </Fragment>
  );
};
export default Login;
