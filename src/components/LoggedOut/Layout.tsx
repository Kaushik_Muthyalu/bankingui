import { Fragment } from "react";
import { NavLink } from "react-router-dom";
import Container from "../common/Layout/Container";
import Navbar from "../common/Navbar";
import BrandLogo from "../common/Navbar/BrandLogo";

type LayoutProps = { children?: any };

const Layout = (props: LayoutProps) => {
  return (
    <Fragment>
      <div style={{ height: "100vw", width: "100hw", position: "fixed" }} className="logout-bg"></div>
      <Navbar id="loggedOutNavbar">
        <Container>
          <BrandLogo>BankingApp</BrandLogo>
          <ul id="nav-mobile" className="right hide-on-med-and-down black-text">
            <li>
              <NavLink to="/">Login</NavLink>
            </li>
            <li>
              <NavLink to="/Register">Register</NavLink>
            </li>
          </ul>
        </Container>
      </Navbar>
      {props.children}
    </Fragment>
  );
};
export default Layout;
