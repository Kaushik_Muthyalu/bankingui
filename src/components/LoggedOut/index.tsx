import { Redirect, Route, Switch } from "react-router";
import Layout from "./Layout";
import Login from "./Login";
import Register from "./Register";

interface LoggedOutProps {
  children?: any;
}

const LoggedOut = (props: LoggedOutProps) => {
  return (
    <Layout>
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route path="/Register">
          <Register />
        </Route>
        <Route>
          <Redirect to="/" />
        </Route>
      </Switch>
    </Layout>
  );
};
export default LoggedOut;
