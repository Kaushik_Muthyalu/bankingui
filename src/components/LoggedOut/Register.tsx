import { useReducer } from "react";
import Button from "../common/Button";
import Card from "../common/Card";
import { Form } from "../common/Form";
import Col from "../common/Layout/Col";
import Container from "../common/Layout/Container";
import Row from "../common/Layout/Row";
import TextInput from "../common/TextInput";
import DatePicker from "../common/DatePicker";
import { Link } from "react-router-dom";
import { formReducer } from "../../reducers/formReducer";
import useAuth from "../../hooks/useAuth";

const Register = () => {
  const [state, dispatch] = useReducer(formReducer, { Name: "", Email: "", Password: "", DOB: "" });

  const auth = useAuth();

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    await auth.register(state);
  };

  return (
    <div className="center-align">
      <Container style={{ margin: "8em auto" }}>
        <Card
          className=" z-depth-1 logout-secondary-bg"
          style={{ display: "inline-block", padding: "32px 48px 0px 48px" }}>
          <Form col={{ s: 12 }} onSubmit={(e) => onSubmit(e)}>
            <Row>
              <Col col={{ s: 12 }}>Enter your credentials</Col>
            </Row>
            <Row>
              <TextInput
                col={{ s: 12 }}
                value={state.Name}
                onChange={(e) => {
                  dispatch({ key: "Name", value: e.target.value });
                }}
                type="text"
                name="name"
                id="name"
                required
                validate>
                Name
              </TextInput>
            </Row>
            <Row>
              <TextInput
                col={{ s: 12 }}
                value={state.Email}
                onChange={(e) => {
                  dispatch({ key: "Email", value: e.target.value });
                }}
                type="email"
                name="email"
                id="email"
                required
                validate>
                Email
              </TextInput>
            </Row>
            <Row>
              <TextInput
                col={{ s: 12 }}
                value={state.Password}
                onChange={(e) => {
                  dispatch({ key: "Password", value: e.target.value });
                }}
                type="password"
                name="password"
                id="password"
                required
                validate>
                Password
              </TextInput>
            </Row>
            <Row>
              <DatePicker
                col={{ s: 12 }}
                value={state.DOB}
                onDateSelect={(date) => {
                  console.log("hi");
                  dispatch({ key: "DOB", value: date.toUTCString() });
                }}
                name="dateOfBirth"
                id="dateOfBirth">
                Date
              </DatePicker>
            </Row>
            <br />
            <div className="center-align">
              <Row>
                <Button name="btn_login" col={{ s: 12 }} large waves wavesLight formNoValidate>
                  Register
                </Button>
              </Row>
            </div>
          </Form>
        </Card>
        <br />
        <Link to="/">Sign in instead</Link>
      </Container>
    </div>
  );
};
export default Register;
