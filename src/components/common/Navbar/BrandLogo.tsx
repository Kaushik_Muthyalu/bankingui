type BrandLogoProps = { className?: string; children: any };

const BrandLogo = (props: BrandLogoProps) => {
  return (
    <a href="/#" className={`brand-logo ${props.className ?? ""}`}>
      {props.children}
    </a>
  );
};
export default BrandLogo;
