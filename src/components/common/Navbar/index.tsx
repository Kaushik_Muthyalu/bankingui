type NavbarProps = { id?: string; className?: string; children?: any };

const Navbar = (props: NavbarProps) => {
  return (
    <nav id={props.id} className={props.className ?? ""}>
      <div className="nav-wrapper">{props.children}</div>
    </nav>
  );
};
export default Navbar;
