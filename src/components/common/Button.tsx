import MaterialProps, { getClass } from "./MaterialProps";

interface ButtonProps extends MaterialProps, React.ButtonHTMLAttributes<HTMLButtonElement> {
  floating?: boolean;
  flat?: boolean;
  large?: boolean;
  small?: boolean;
}

const getButtonClass = (props: ButtonProps) => {
  let className = "";

  if (props.floating) className += "btn-floating ";
  else if (props.flat) className += "btn-flat ";
  else className += "btn ";

  if (props.large) className += "btn-large ";
  if (props.small) className += "btn-small ";

  return className;
};

const Button = (props: ButtonProps) => {
  let className = getButtonClass(props);
  className += getClass(props);

  return (
    <button {...props} className={className}>
      {props.children}
    </button>
  );
};
export default Button;
