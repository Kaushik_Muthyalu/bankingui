import Color, { getClassName as getColorClass } from "./Color";
import Grid, { getClassName as getGridClass } from "./Grid";
import Waves, { getClassName as getWaveClass } from "./Waves";

export default interface MaterialProps extends Color, Grid, Waves {
  className?: string;
}

export const getClass = (props: MaterialProps) => {
  let className = props.className ?? "";
  className += " " + getColorClass(props);
  className += " " + getGridClass(props);
  className += " " + getWaveClass(props);
  return className;
};
