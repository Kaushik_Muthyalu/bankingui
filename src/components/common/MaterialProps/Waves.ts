export const getClassName = (waves: Waves) => {
  return `${waves.waves ? "waves-effect" : ""} ${waves.wavesLight ? "waves-light" : ""}`;
};

export default interface Waves {
  waves?: Boolean;
  wavesLight?: Boolean;
}
