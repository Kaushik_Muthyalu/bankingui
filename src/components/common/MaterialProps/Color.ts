type baseColor =
  | "red"
  | "pink"
  | "purple"
  | "deep-purple"
  | "indigo"
  | "blue"
  | "light-blue"
  | "cyan"
  | "teal"
  | "green"
  | "light-green"
  | "lime"
  | "yellow"
  | "amber"
  | "orange"
  | "deep-orange"
  | "brown"
  | "grey"
  | "blue-grey"
  | "black"
  | "white"
  | "transparent"
  | undefined;

type tweak = 1 | 2 | 3 | 4 | 5 | undefined;

type color = {
  name?: baseColor;
  darken?: tweak;
  lighten?: tweak;
  accent?: tweak;
};

const getColorClass = (color?: color, text = false) => {
  let className = "";
  if (color) {
    className += ` ${color.name}${text ? "-text" : ""}`;
    className += ` ${text ? "text-" : ""}${color.darken ? "darken-" + color.darken : ""}`;
    className += ` ${text ? "text-" : ""}${color.darken ? "darken-" + color.darken : ""}`;
    className += ` ${text ? "text-" : ""}${color.darken ? "darken-" + color.darken : ""}`;
  }
  return className;
};

export const getClassName = (color: Color) => {
  let className = "";
  className += getColorClass(color.background);
  className += getColorClass(color.text, true);
  return className;
};

export default interface Color {
  background?: color;
  text?: color;
}
