import React from "react";
import MaterialProps from "./MaterialProps";
import { getClassName as getGridClass } from "./MaterialProps/Grid";

interface TextInputProps extends MaterialProps, React.InputHTMLAttributes<HTMLInputElement> {
  labelText?: string;
  containerClassName?: string;
  validate?: boolean;
}

const TextInput = (props: TextInputProps) => {
  let gridClass = getGridClass(props);
  return (
    <div className={`input-field ${gridClass} ${props.containerClassName ?? ""}`}>
      <input {...props} className={`${props.validate ? "validate" : ""} ${props.className}`} children={undefined} />
      <label htmlFor={props.id}>{props.children}</label>
    </div>
  );
};

export default TextInput;
