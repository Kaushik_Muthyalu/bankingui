import MaterialProps, { getClass } from "./MaterialProps";

interface ReportProps extends MaterialProps, React.HTMLAttributes<HTMLDivElement> {}

const Report = (props: ReportProps) => {
  let className = getClass(props);
  return <div {...props} className={`report ${className} ${props.className}`}></div>;
};
export default Report;
