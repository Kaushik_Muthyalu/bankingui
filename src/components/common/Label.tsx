import React from "react";
import MaterialProps, { getClass } from "./MaterialProps";

interface LabelProps extends MaterialProps, React.LabelHTMLAttributes<HTMLLabelElement> {}

const Label = (props: LabelProps) => {
  let className = getClass(props);
  return <label {...props} className={`${className} ${props.className}`}></label>;
};

export default Label;
