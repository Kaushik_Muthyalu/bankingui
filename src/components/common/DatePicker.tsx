import React, { useEffect, useRef } from "react";
import MaterialProps from "./MaterialProps";
import M from "materialize-css";
import { getClassName as getGridClass } from "./MaterialProps/Grid";

interface DatePickerProps extends MaterialProps, React.InputHTMLAttributes<HTMLInputElement> {
  labelText?: string;
  containerClassName?: string;
  validate?: boolean;
  onDateSelect?: (selectedDate: Date) => void;
}

const DatePicker = (props: DatePickerProps) => {
  let gridClass = getGridClass(props);

  const elem = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (elem.current) {
      M.Datepicker.init(elem.current, { onSelect: props.onDateSelect });
    }
  });

  return (
    <div className={`input-field ${gridClass} ${props.containerClassName ?? ""}`}>
      <input ref={elem} {...props} type="text" className={`datepicker ${props.className}`} children={undefined} />
      <label htmlFor={props.id}>{props.children}</label>
    </div>
  );
};

export default DatePicker;
