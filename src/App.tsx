import React from "react";
//import logo from "./logo.svg";
import "./styles/banking.scss";
import LoggedOut from "./components/LoggedOut";
import { BrowserRouter as Router } from "react-router-dom";
import LoggedIn from "./components/LoggedIn";
import { AuthContext, AuthProvider } from "./context/AuthContext";

const App = () => {
  const loggedIn = true;
  //return <Router>{loggedIn ? <LoggedIn /> : <LoggedOut />}</Router>;
  return (
    <Router>
      <AuthProvider>
        <AuthContext.Consumer>
          {({ state, dispatch }) => {
            return state.loggedIn ? <LoggedIn /> : <LoggedOut />;
          }}
        </AuthContext.Consumer>
      </AuthProvider>
    </Router>
  );
};

export default App;
