import { createContext, useContext, useEffect, useReducer } from "react";
import { refreshToken } from "../api/authenticate";
import useAuth from "../hooks/useAuth";
import authReducer, { Action, stateType } from "../reducers/authReducer";

const initialState: stateType = {
  loggedIn: false,
};

export const AuthContext = createContext<{ state: stateType; dispatch: React.Dispatch<Action> }>({
  state: initialState,
  dispatch: () => null,
});

//export const refresh = (refresh_token: "") => {
//const authContext = useContext(AuthContext);
//return refreshToken(authContext.state.refresh_token ?? refresh_token).then((result) => {
//return result;
//});
//};

export const AuthProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  const auth = useAuth();

  useEffect(() => {
    auth.init(dispatch);
  }, []);

  return <AuthContext.Provider value={{ state, dispatch }}>{children}</AuthContext.Provider>;
};
