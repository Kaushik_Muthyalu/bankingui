export interface stateType {
  loggedIn: boolean;
  access_token?: string;
  expiry?: number;
  refresh_token?: string;
}

type actionType = "login" | "logout";

export type payloadType = {
  Success: boolean;
  AccessToken: string;
  Expiry: string;
  RefreshToken: string;
  Error: string;
};

export interface Action {
  type: actionType;
  payload?: payloadType;
}

export default function authReducer(state: stateType, action: Action): stateType {
  const payload = action.payload;
  switch (action.type) {
    case "login":
      if (payload?.Success)
        return {
          loggedIn: true,
          access_token: payload.AccessToken,
          expiry: Date.parse(payload.Expiry),
          refresh_token: payload.RefreshToken,
        };
      return { loggedIn: false };
    case "logout":
      return { loggedIn: false };
  }
}
