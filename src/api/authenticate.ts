import axios from "axios";
import { payloadType } from "../reducers/authReducer";
import { apiConfig, withToken } from "./apiConfig";

export interface loginDetails {
  Email: string;
  Password: string;
}

export const authenticate = async (login: loginDetails): Promise<payloadType> =>
  await axios
    .create(apiConfig)
    .post("Auth/Authenticate", login)
    .then((response) => {
      return response.data;
    });

export const refreshToken = async (token: string): Promise<payloadType> =>
  await axios
    .create(withToken(token))
    .post("Auth/Refresh")
    .then((response) => {
      return response.data;
    });
