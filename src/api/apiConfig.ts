import { AxiosRequestConfig } from "axios";

export const apiConfig: AxiosRequestConfig = {
  baseURL: `${process.env.REACT_APP_BACKEND_API_BASE_URL}/api`,
};

export const withToken = (token: string, config = apiConfig): AxiosRequestConfig => {
  return {
    ...config,
    headers: {
      ...config.headers,
      Authorization: `Bearer ${token}`,
    },
  };
};
