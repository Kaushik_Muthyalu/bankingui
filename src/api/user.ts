import axios from "axios";
import { apiConfig, withToken } from "./apiConfig";

export const checkUserExists = async (email: string) => {
  let result = await axios
    .create(apiConfig)
    .get("User/CheckIfUserExists", {
      params: { email: email },
    })
    .then((response) => {
      return response.data;
    });
  return result;
};

export const getUserInfo = async (token: string) => {
  let result = await axios
    .create(withToken(token))
    .get("User")
    .then((response) => {
      return response.data;
    });
  return result;
};
