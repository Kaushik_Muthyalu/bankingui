import axios from "axios";
import { withToken } from "./apiConfig";

export const getAllTransactions = async (token: string) => {
  let result = await axios
    .create(withToken(token))
    .get("Transaction")
    .then((response) => {
      return response.data;
    });
  return result;
};

interface createTransaction {
  Amount: number;
}

export const deposit = async (token: string, transaction: createTransaction) =>
  await axios
    .create(withToken(token))
    .post("Transaction/Deposit", transaction)
    .then((response) => {
      if (response.status === 201) return response.data;
      return null;
    });

export const withdraw = async (token: string, transaction: createTransaction) =>
  await axios
    .create(withToken(token))
    .post("Transaction/Withdraw", transaction)
    .then((response) => {
      if (response.status === 201) return response.data;
      return null;
    });
