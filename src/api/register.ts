import axios from "axios";
import { apiConfig } from "./apiConfig";

export interface registrationdetails {
  Name: string;
  Email: string;
  Password: string;
  DOB: string;
}

export const registerUser = async (registration: registrationdetails) =>
  await axios
    .create(apiConfig)
    .post("auth/register", registration)
    .then((response) => {
      return response.data;
    });
